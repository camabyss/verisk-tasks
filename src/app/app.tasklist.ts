import { Component } from '@angular/core';

@Component({
	selector: 'task-list',
	templateUrl: './app.tasklist.html',
})

export class TaskList {
	
	items : Array<string> = [];
	filteredItems : Array<string> = [];
	sort : boolean  = false;
	searchTerm : string = "";
	recentUpdate : number = 0;

	constructor() {
		this.initData();

		// This piece of code automatically updates the app over many browser windows
		setInterval(() => {
			if (localStorage.hasOwnProperty('verisk-timer')) {
				const newUpdate = Number(localStorage.getItem('verisk-timer'));
				if (this.recentUpdate < newUpdate) {
					this.recentUpdate = newUpdate;
					this.initData();
				}
			}
		}, 500);
	}
	initData() {
		if (localStorage.hasOwnProperty('verisk-todo'))
			this.items = JSON.parse(localStorage.getItem('verisk-todo'));
		this.filterItems();
	}
	updateItems() {
		this.recentUpdate = Date.now();
		localStorage.setItem('verisk-todo', JSON.stringify(this.items));
		localStorage.setItem('verisk-timer', String(this.recentUpdate));
		this.filterItems();
	}

	removeItem(item : string) {
		const index = this.items.indexOf(item);
		this.items.splice(index, 1);
		this.updateItems();
	}
	addItem(item : string) {
		this.items.unshift(item);
		this.updateItems();
	}

	search() {
		this.filterItems();
	}
	toggleSort() {
		if (this.sort === false)
			this.sort = true;
		else this.sort = false;
		this.filterItems();
	}

	filterItems() {
		const filteredItems = [];
		for (let item of this.items) {
			if (! this.searchTerm ||
				item.indexOf(this.searchTerm) !== -1) {
				filteredItems.push(item);
			}
		}
		if (this.sort === true)
			filteredItems.sort();
		this.filteredItems = filteredItems;
	}
}
