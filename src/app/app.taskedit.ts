import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'task-edit',
  templateUrl: './app.taskedit.html',
})
export class TaskEdit {
	value : string = "";
	@Output() save = new EventEmitter();
	saveTask = () => {
		if (this.value.length) {
			this.save.next(this.value);
			this.value = "";
		}
	}
}
