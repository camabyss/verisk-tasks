import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'task-item',
  templateUrl: './app.taskitem.html',
})
export class TaskItem {
	@Output() remove = new EventEmitter();
}
