import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskList } from './app.tasklist';
import { TaskItem } from './app.taskitem';
import { TaskEdit } from './app.taskedit';

@NgModule({
  declarations: [
    AppComponent,
    TaskList,
    TaskItem,
    TaskEdit
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
